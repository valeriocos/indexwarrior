#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Luis Cañas-Díaz <lcanas@bitergia.com>
#

import logging
import sys

from indexwarrior.main import IndexWarrior
from indexwarrior.command import IndexWarriorParser
from indexwarrior.errors import ParserException

if '..' not in sys.path:
    sys.path.insert(0, '..')


def main(stdin_args):
    """Main method"""

    logging.getLogger('elasticsearch').setLevel(logging.ERROR)

    iwparser = IndexWarriorParser()
    try:
        args = iwparser.parse(*stdin_args)

        iwarrior = IndexWarrior(args.es_host, args.es_timeout)
        iwarrior.run(args)
    except ParserException as e:
        s = "\n%s\n"
        sys.stdout.write(s % e.message)
        sys.exit(2)


if __name__ == '__main__':
    main(sys.argv[1:])
